function obtenerUsuarios() {
   /* fetch("http://localhost:3000/api/todosUsuarios",{method: 'GET'})
        .then((response) => {
            console.log("Estas es la respuesta del then", response);
            debugger
            response.json()})
        .then((data) => {
            console.log("Esta es la respuesta del segundo then",data)});*/

            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
              };
              
              fetch("http://localhost:3000/api/todosUsuarios", requestOptions)
                .then(response => response.text())
                .then(result => {
                    let datos = JSON.parse(result)
                    console.log(datos);
                    
                    const table = document.getElementById("alumnos");

                    datos.forEach((e,i) => {    //< ---  recorremos data
                    
                      let tr = document.createElement("tr"); //< ---  creamos una fila
                      
                      let td = document.createElement("td"); //< ---  Hacemos columna index dentro de la fila
                      td.classList.add("index");
                      td.innerHTML = i;
                      tr.appendChild(td); //< --- Agregamos la columna en la fila
                    
                      for (p in e) {  //< ---  recorremos cada propiedad de cada elemento
                    
                        let td = document.createElement("td"); //< ---  Hacemos columna dentro de la fila
                        td.classList.add(p);//<-- le podemos agregar a toda la columna la misma clase
                        td.innerHTML = e[p]; 
                    
                        tr.appendChild(td); //< --- Agregamos la columna en la fila
                    
                      }
                    
                      table.appendChild(tr); //< --- Agregamos la fila a la tabla
                    
                    });   

                })
                .catch(error => console.log('error', error));
}